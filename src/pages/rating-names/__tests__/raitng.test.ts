import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture } from '../__page-object__';

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page   }) => {
  await page.route(
    'https://meowle.fintech-qa.ru/api/likes/cats/rating',
    async route => {
      await route.fulfill({
        status: 500,
      });

    }
  );
  await page.goto('https://meowle.fintech-qa.ru/rating');
  await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
});

test('Рейтинг котиков отображается по убыванию', async ({ page, ratingPage }) => {
  // Открываем страницу рейтинга
  await ratingPage.openRatingPage();

  // Получаем все элементы
  const likes = await ratingPage.getElements();

  // Проверяем, что элементы отображаются в порядке убывания
  ratingPage.checkLikesDescending(likes);
});