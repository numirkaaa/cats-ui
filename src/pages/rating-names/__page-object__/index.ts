import type { Page, TestFixture } from '@playwright/test';
import { expect,test } from '@playwright/test';

export class RatingPage {
  private page: Page;
  public ratingSelector: string

  constructor({page}: {page: Page;}) {
    this.page = page;
    this.ratingSelector = '//td[@class="rating-names_item-count__1LGDH has-text-success"]'
  }


  async openRatingPage() {
    return await test.step('Открыть страницу "Рейтинг имён котиков"', async () => {
      await this.page.goto('/rating')
    })
  }

  async getElements() {
    const elements = await this.page
      .locator(this.ratingSelector)
      .allTextContents();
    return elements.map(el => parseInt(el));
  }

  checkLikesDescending(likes: number[]): void {
    for (let i = 0; i < likes.length - 1; i++) {
      if (likes[i] < likes[i + 1]) {
        throw new Error(`Ошибка: Рейтинг не по убыванию на позиции ${i + 1}. Значение ${likes[i]} меньше значения ${likes[i + 1]}`);
      }
      expect(likes[i]).toBeGreaterThanOrEqual(likes[i + 1]);
    }
  }
}

export type RatingPageFixture = TestFixture<
  RatingPage,
  {
    page: Page;
  }
>;

export const ratingPageFixture: RatingPageFixture = async ({ page }, use) => {
  const ratingPage = new RatingPage({ page });
  await use(ratingPage);
};

